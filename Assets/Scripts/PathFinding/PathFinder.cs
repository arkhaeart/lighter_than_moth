﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading;
using GameBoard;
namespace Pathfinding
{
    public class PathFinder : Singleton<PathFinder>
    {
        Queue<PathResult> pathResults=new Queue<PathResult>();
        private void Start()
        {
            StartCoroutine(CheckReadiedPaths());
        }
        IEnumerator CheckReadiedPaths()
        {
            while (true)
            {
                if(pathResults.Count>0)
                {
                    ProcessPathResult();
                }
                yield return null;
            }
        }
        public void OnPathRequested(PathRequest request)
        {
            ThreadStart newThread = delegate { ProcessPathRequest(request); };
            newThread.Invoke();
        }
        void PathFound(PathResult result)
        {
            lock(pathResults)
            {
                pathResults.Enqueue(result);
            }
        }
        void ProcessPathResult()
        {
            lock(pathResults)
            {
                PathResult result = pathResults.Dequeue();
                result.callback(result.path);
            }
        }
        void ProcessPathRequest(PathRequest request)
        {
            bool success = false;
            Vector3[] waypoints = new Vector3[0];
            Node starting = Board.Instance.GetNodeFromPos(request.start);
            Node ending = Board.Instance.GetNodeFromPos(request.end);
            if (starting == null || ending == null)
                return;
            starting.parent = starting;
            HeapQueue<Node> open = new HeapQueue<Node>(Board.Instance.GetTileCount());
            HashSet<Node> closed = new HashSet<Node>();
            open.Enqueue(starting);
            int i = 0;
            while (open.Count > 0)
            {
                
                i++;
                if (i >= 10000)
                    break;
                Node current = open.Dequeue();
                closed.Add(current);
                if (current == ending)
                {
                    success = true;
                    break;
                }
                var neighbours = Board.Instance.GetNeighbours(current);
                foreach (var neighbour in neighbours)
                {
                    
                    if (neighbour == null || closed.Contains(neighbour))
                        continue;
                    int newGCost = current.gCost + GetDistance(current, neighbour);
                    if (newGCost < neighbour.gCost || !open.Contains(neighbour))
                    {
                        neighbour.gCost = newGCost;
                        neighbour.hCost = GetDistance(neighbour, ending);
                        neighbour.parent = current;
                        if (!open.Contains(neighbour))
                            open.Enqueue(neighbour);
                        else
                            open.Update(neighbour);
                    }
                }
            }
            if (success)
            {
                PathResult result = new PathResult(ReconstructPath(starting, ending), request.callback);
                PathFound(result);
            }
            else
                Debug.Log("pathfinding unsuccessful!");
            return;
        }
        
        int GetDistance(Vector3 from, Vector3 to)
        {
            int X = Mathf.RoundToInt((to.x - from.x) / 0.16f);
            int Y = Mathf.RoundToInt((to.y - from.y) / 0.16f);

            return Mathf.Abs(X) + Mathf.Abs(Y);
        }
        Vector3[] ReconstructPath(Node from,Node to)
        {
            List<Vector3> path = new List<Vector3>();
            Node current = to;
            while(current!=from)
            {
                path.Add(current.position);
                current = current.parent;
            }
            path.Reverse();
            return path.ToArray();

        }
        int GetDistance(Node from, Node to)
        {
            int distance = Mathf.RoundToInt(Vector3.Distance(from.position, to.position)/0.16f);
            int X = Mathf.Abs(from.X + to.X);
            int Y = Mathf.Abs(from.Y + to.Y);
            return distance;
        }
        public struct PathResult
        {
            public Vector3[] path;
            public Action<Vector3[]> callback;

            public PathResult(Vector3[] path, Action<Vector3[]> callback)
            {
                this.path = path;
                this.callback = callback;
            }

        }

        public struct PathRequest
        {
            public Vector3 start;
            public Vector3 end;
            public Action<Vector3[]> callback;

            public PathRequest(Vector3 _start, Vector3 _end, Action<Vector3[]> _callback)
            {
                start = _start;
                end = _end;
                callback = _callback;
            }
        }
    }
}