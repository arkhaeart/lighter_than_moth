﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameBoard;
using Action = System.Action<bool>;

public abstract class LightSeeing
{
    public MovingAgent agent;
    protected bool seeLight = false;
    public delegate void SawLight(ShiningTile tile);
    protected abstract IEnumerator CheckLight();

    public LightSeeing(MovingAgent agent)
    {
        this.agent = agent;
        agent.StartCoroutine(CheckLight());
    }
}
public class LightFollow : LightSeeing
{
    Action healthUpdate;
    event SawLight OnSawLight;
    public LightFollow(MovingAgent agent,SawLight sawLight,Action healthUpdate):base(agent)
    {
        OnSawLight += sawLight;
        this.healthUpdate = healthUpdate;
    }

    protected override IEnumerator CheckLight()
    {
        yield return null;
        while (true)
        {
            FloorTile current = Board.GetTileFromPos<FloorTile>(agent.transform.position);
            if (current.shiningTile != null)
            {

                seeLight = true;
                OnSawLight?.Invoke(current.shiningTile);
            }
            else
                seeLight = false;
            healthUpdate(seeLight);
            yield return new WaitForSeconds(0.3f);
        }
    }
}

public class LightAvoid : LightSeeing
{
    //event SawLight OnSawLight;
    public LightAvoid(MovingAgent agent) : base(agent)
    {

    }

    protected override IEnumerator CheckLight()
    {
        yield return null;
    }
}
