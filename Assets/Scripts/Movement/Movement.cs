﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Movement
{
    public class MoveModule
    {
        protected MonoBehaviour mono;
        protected Rigidbody2D rb2d;
        public MoveModule(Rigidbody2D _rb2d,MonoBehaviour _mono)
        {
            rb2d = _rb2d;
            mono = _mono;
        }
        public virtual void MoveTo(Vector2 direction)
        {
            mono.StartCoroutine(SmoothMovement(direction));
        }
        protected virtual IEnumerator SmoothMovement(Vector2 direction)
        {
            
            Vector2 target = rb2d.position + direction * 0.16f;
            while (rb2d.position != target)
            {

                Vector2 newPos = rb2d.position + direction * 0.16f / 4;
                rb2d.MovePosition(newPos);
                yield return null;
            }
        }
    }

    public interface IMovingAgent
    {
        void Move();
    }
}
