﻿using UnityEngine;
using UnityEditor;

public class SystemsUnpacker
{
    const string path="Prefabs/System/";
    [MenuItem("File/UnpackSystems")]
    public static void UnpackSystems()
    {
        Object[] prefabs = GetPrefabs();
        foreach(var p in prefabs)
        {
            GameObject newObj=GameObject.Instantiate(p, Vector3.zero, Quaternion.identity) as GameObject;
        }
    }
    static Object[]GetPrefabs()
    {
        return Resources.LoadAll(path);
    }
}