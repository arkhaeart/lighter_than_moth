﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pit : FloorTile
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Agent obj = collision.GetComponent<Agent>();
        if(obj is MovingAgent)
        {
            MovingAgent agent = (MovingAgent)obj;
            if (agent.type == MovingAgent.CreatureType.WALKING)
                agent.gameObject.SetActive(false);
        }
        else if(obj is GameTile)
        {
            return;
        }
        else
        {
            obj.gameObject.SetActive(false);
        }
    }
    public override void ChangeColor(float ratio)
    {
        return;
    }
}
