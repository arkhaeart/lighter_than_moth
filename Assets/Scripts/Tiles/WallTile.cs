﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallTile : GameTile
{
    public WallType wallType;
}
public enum WallType
{
    DEFAULT,
    SEMI_TRANSPARENT,
    TRANSPARENT
}