﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameBoard;

public abstract class GameTile : Agent
{
    delegate void OnPositionChanged(GameTile tile, Vector3 newDimensions);
    event OnPositionChanged onPositionChanged;
    SpriteRenderer render;
    public Vector3 Dimensions
    {
        get
        {
            System.Tuple<int,int> tuple=transform.position.GetTileCoord();
            return new Vector3(tuple.Item1, tuple.Item2);
        }
        set
        {
            onPositionChanged(this, value);
            StartCoroutine(SmoothMovement(value));

        }

    }
    protected override void Awake()
    {
        base.Awake();
        render = GetComponent<SpriteRenderer>();
        render.color = Color.HSVToRGB(0, 0, 0.33f);
        onPositionChanged += Board.OnTileMoved;
    }
    public virtual void ChangeColor(float ratio)
    {
        render.color = Color.HSVToRGB(0, 0, ratio);
    }
    private void OnDisable()
    {
        onPositionChanged -= Board.OnTileMoved; 
    }
}

