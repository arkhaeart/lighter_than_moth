﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mirror : WallTile
{
    public int direction;
    public Sprite[] sprites;
    public float Angle
    {
        get
        {
            float gr=direction * 45;
            gr = gr > 180 ? gr - 360 : gr;
            return gr;
        }
    }
    public Vector3 VectorDir
    {
        get
        {
            Quaternion quat = Quaternion.AngleAxis(Angle, Vector3.forward);
            return quat * Vector3.forward;
        }
    }
    public int Direction
    {
        get => direction;
        set
        {
            GetComponent<SpriteRenderer>().sprite = sprites[value];
            direction = value;
        }
    }
    public Vector3 Reflect(Vector3 input)
    {
        float locAngle = Vector3.Angle(VectorDir, input);
        Quaternion quat = Quaternion.AngleAxis(locAngle, VectorDir);
        Vector3 reflection = quat * input;
        Debug.Log(reflection);
        return reflection;
    }
}
