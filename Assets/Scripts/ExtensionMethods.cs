﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using GameBoard;
public static class ExtensionMethods 
{
    public static Tuple<int, int> GetTileCoord(this Vector3 pos)
    {

        int _x = Mathf.RoundToInt((pos.x - Board.xOffset) / 0.16f);
        int _y = Mathf.RoundToInt((pos.y - Board.yOffset) / 0.16f);

        return Tuple.Create(_x, _y);
    }
    public static Vector3 Round(this Vector3 pos)
    {
        return new Vector3((float)Math.Round(pos.x, 2), (float)Math.Round(pos.y, 2), (float)Math.Round(pos.z, 2));
    }

}
