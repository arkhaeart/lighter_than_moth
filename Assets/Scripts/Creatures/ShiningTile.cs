﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameBoard;

public class ShiningTile
{
    public delegate void OnStopShine();
    public static event OnStopShine onStopShine;
    public static List<ShiningTile> pool = new List<ShiningTile>();
    public static Queue<ShiningTile> inactive = new Queue<ShiningTile>();
    public static int xLength, yLength;
    public ShiningTile parent;
    public float intensity;
    public float Intensity
    {
        get { return intensity; }
        set
        {

            intensity = value;
            if (value > 0 && !walltile)
            {
                Emit();
            }
        }
    }
    public int x, y;
    bool walltile = false;
    List<ShiningTile> children = new List<ShiningTile>();

    ShiningTile TryTakeFromPool()
    {
        if (inactive.Count > 0)
        {
            ShiningTile qTile = inactive.Dequeue();
            onStopShine += qTile.SaveToPool;
            return qTile;
        }
        ShiningTile newTile = new ShiningTile();
        pool.Add(newTile);
        onStopShine += newTile.SaveToPool;
        return newTile;
    }
    void SaveToPool()
    {
        onStopShine -= SaveToPool;
        Intensity = 0;
        walltile = false;
        Board.LightResponse(this);
        inactive.Enqueue(this);
    }
    public static void Stop()
    {
        onStopShine?.Invoke();
    }
    /// <summary>
    /// Instantiating of the very first shining tile and setting all the requirements
    /// </summary>
    /// <param name="position"></param>
    /// <param name="brightness"></param>
    public ShiningTile(Vector3 position, float brightness)
    {
        pool.Clear();
        xLength = Board.Instance.xLength;
        yLength = Board.Instance.yLength;
        
        System.Tuple<int, int> tuple = position.GetTileCoord();
        x = tuple.Item1;
        y = tuple.Item2;
        pool.Add(this);
        intensity = brightness;

    }
    public ShiningTile()
    {

    }
    public void Init(Vector3 position)
    {
        System.Tuple<int, int> tuple = position.GetTileCoord();
        x = tuple.Item1;
        y = tuple.Item2;
        parent = null;
        Intensity = 4;
    }
    void Emit()
    {
        if (Board.gameBoard[x, y, 0] == null)
        {
            WallTile tile = (WallTile)Board.gameBoard[x, y, 1];
            if (tile == null)
                return;
            switch (tile.wallType)
            {
                case WallType.DEFAULT:
                    return;
            }
            PostEmit(tile);
            return;
        }
        if (parent != null)
        {
            if (!parent.walltile)
            {
                bool blocked = Board.CheckLight(Board.gameBoard[parent.x, parent.y, 0].transform.position, Board.gameBoard[x, y, 0].transform.position);
                if (blocked)
                    return;
            }
        }
        Board.LightResponse(this);
        if (parent == null)
        {

            for (int i = -1; i <= 1; i++)
            {
                for (int z = -1; z <= 1; z++)
                {
                    if (!CheckInArea(x + i, y + z) || i == 0 && z == 0)
                        continue;
                    ShiningTile child = TryTakeFromPool();
                    child.parent = this;
                    child.x = x + i;
                    child.y = y + z;
                    if (i * z == 0)
                        child.Intensity = intensity - 1;
                    else
                        child.Intensity = intensity - 1.4f;
                }
            }
            for (int i = -2; i <= 2; i++)
            {
                for (int z = -2; z <= 2; z++)
                {
                    if (Mathf.Abs(i) == 1 && Mathf.Abs(z) == 2 || Mathf.Abs(i) == 2 && Mathf.Abs(z) == 1)
                    {
                        if (!CheckInArea(x + i, y + z))
                            continue;
                        ShiningTile child = TryTakeFromPool();
                        child.parent = this;
                        child.x = x + i;
                        child.y = y + z;
                        child.Intensity = intensity - 2.4f;
                    }
                }
            }
        }
        else
        {
            int _x = 2 * x - parent.x;
            int _y = 2 * y - parent.y;
            float desInt = 2 * intensity - parent.intensity;
            if (!CheckInArea(_x, _y) || desInt < 1)
                return;
            ShiningTile child = TryTakeFromPool();
            child.parent = this;
            child.x = _x;
            child.y = _y;
            child.Intensity = desInt;
        }

    }
    bool CheckInArea(int x, int y)
    {
        if (x < 0 || x > xLength - 1 || y < 0 || y > yLength - 1)
            return false;
        else return true;
    }
    void PostEmit(WallTile tile)
    {
        walltile = true;
        int _x, _y;
        _x = 2 * x - parent.x;//x=x-(
        _y = 2 * y - parent.y;
        float mod = 0;
        float cmod = parent.intensity- intensity ;
        if (tile is Prisma pTile)
        {
            mod = intensity + cmod * pTile.focus / 7;
        }
        else if (tile is Mirror mTile)
        {
            Vector3 reflection = mTile.Reflect(new Vector3(x - parent.x, y - parent.y));
            _x = Mathf.RoundToInt(reflection.x);
            _y = Mathf.RoundToInt(reflection.y);
            mod = parent.intensity;
        }
        else
        {
            switch ((int)tile.wallType)
            {
                case 1:
                    mod = intensity;
                    break;
                case 2:
                    mod = parent.intensity;
                    break;
            }
        }
        Intensity = mod;
        if (!CheckInArea(_x, _y))
            return;
        ShiningTile newTile = TryTakeFromPool();
        newTile.x = _x;
        newTile.y = _y;
        newTile.parent = this;
        newTile.Intensity = intensity + cmod;
    }

}
