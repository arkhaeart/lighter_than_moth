﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Creatures
{
    public class Lamp : MovingAgent, IPlayable
    {
        public float brightness;
        Coroutine shining, awaiting;
        GameTile lastTile;
        ShiningTile mainShine;
        private void Start()
        {

            mainShine = new ShiningTile(transform.position,brightness);
            shining = StartCoroutine(Shine());

        }
        // Update is called once per frame
        void FixedUpdate()
        {

            float h = Input.GetAxisRaw("Horizontal");
            float v = Input.GetAxisRaw("Vertical");
            if (h != 0 || v != 0)
            {
                Vector3 direction = new Vector3(h, v, 0f);
                if (movement == null)
                    movement = StartCoroutine(DiscreteMovement(direction));
            }
        }

        protected override void Move(Vector3 direction)
        {
            if (shining != null)
            {
                StopCoroutine(shining);
            }
            base.Move(direction);

        }
        protected override IEnumerator SmoothMovement(Vector3 direction)
        {
            yield return StartCoroutine(base.SmoothMovement(direction));
            if (mainShine != null)
                ShiningTile.Stop();
            shining = StartCoroutine(Shine());
        }
        IEnumerator Shine()
        {
            while (true)
            {
                if (mainShine != null)
                    ShiningTile.Stop();
                mainShine.Init(transform.position);
                yield return new WaitForSeconds(1f);
            }
        }
        public void ArrivedToExit()
        {
            StartCoroutine(AwaitAnother());
        }
        public IEnumerator AwaitAnother()
        {
            onExit++;
            yield return new WaitForSeconds(1f);
            while (transform.position == LevelManager.Instance.ExitPosition)
            {
                if (onExit == 2)
                {
                    onExit = 0;
                    GameManager.Instance.NextLevel();
                    yield break;
                }
                yield return null;
            }
            onExit--;
        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            Agent agent = collision.GetComponent<Agent>();
            if (agent is Monster monster)
            { }
            else if (agent is Bonus bonus)
            {
                bonus.Effect(OnBonusReceived);
            }
        }
        void OnBonusReceived(BonusType bonusType)
        {

        }
    }
}
 
