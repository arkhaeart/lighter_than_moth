﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ObjectToSave : Agent
{
    protected override void Awake()
    {
        base.Awake();
        SaveLoadManager.onGameSaved += Save;
    }
    protected virtual void OnDisable()
    {
        SaveLoadManager.onGameSaved -= Save;
    }
    public int ID;
    public System.Tuple<int, int> Dimensions
    {
        get { return transform.position.GetTileCoord(); }
    }
    public void Save()
    {
        SaveLoadManager.Instance.objects.Add(this);
    }
}
