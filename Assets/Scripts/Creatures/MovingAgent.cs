﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingAgent : ObjectToSave
{
    protected Coroutine movement;
    protected static int onExit = 0;
    [SerializeField] protected float speed;
    public enum CreatureType
    {
        FLYING,
        WALKING,
        BIG
    }
    public CreatureType type;

    protected virtual IEnumerator DiscreteMovement(Vector3 direction)
    {
        Move(direction);
        yield return new WaitForSeconds(5f / speed);
        movement = null;
    }

    protected virtual void Move(Vector3 direction)
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, 0.142f);
        if(hit.transform!=null)
        {
            if(hit.transform.CompareTag("Wall"))
                return;
            
            else 
            {
                Agent agent = hit.transform.GetComponent<Agent>();
                if (agent is Garbage garbage)
                {
                    if (type == CreatureType.BIG)
                    {

                        if (!garbage.Push(direction))
                            return;
                    }
                }
            }
        }
        StartCoroutine(SmoothMovement(direction));

    }
    protected  override IEnumerator SmoothMovement(Vector3 direction)
    {
        Vector3 target = transform.position + direction * 0.16f;
        if (!GameBoard.Board.CheckWalkable(target.x, target.y))
        {
            goto M;
        }
        if(this is IPlayable player)
        {
            if(target==LevelManager.Instance.ExitPosition)
            {
                
                StartCoroutine(player.AwaitAnother());
            }
        }
        yield return StartCoroutine(base.SmoothMovement(direction));
        yield break;
    M:
        yield return null;
        

    }
}
public interface IPlayable
{
    void ArrivedToExit();
    IEnumerator AwaitAnother();
    
}
