﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
namespace Creatures
{

    public abstract class MonsterState 
    {
        public MonsterState(Monster obj, Func<IEnumerator> func)
        {
            this.obj = obj;
            OnToggle += func;
        }
        public static IEnumerable <Func<IEnumerator>> GetFuncs(MonsterState state)
        {
            foreach(Func<IEnumerator> f in state.OnToggle.GetInvocationList())
            {
                yield return f;
            }
        }
        protected Monster obj;
        protected Func<IEnumerator> func0;
        protected event Func<IEnumerator> OnToggle;
        protected Coroutine coroutine;
        public virtual void ToggleOff()
        {
            try
            {
                obj.StopCoroutine(coroutine);
            }
            catch(Exception e)
            {
                Debug.Log(e.Source);
                Debug.Log(GetType().ToString());
                throw e;
            }

        }

        public virtual void ToggleOn()
        {
            coroutine=obj.StartCoroutine(OnToggle());
        }
    }
    public class IdleState :MonsterState
    {
        public IdleState (Monster obj,Func<IEnumerator> func=null):base(obj,func)
        {
          
        }

        public override void ToggleOff()
        {
            
        }

        public override void ToggleOn()
        {
           
        }
    }
    public class PatrolState : MonsterState
    {

        public PatrolState(Monster obj,Func<IEnumerator> patrolling):base(obj,patrolling)
        {
        }

    }
    public class ChaseState : MonsterState
    {
        public ChaseState(Monster obj, Func<IEnumerator> chasing):base(obj,chasing)
        {  
        }

    }
    public class SeekState : MonsterState
    {
        public SeekState(Monster obj, Func<IEnumerator> seeking) : base(obj,seeking)
        {
        }
    }
    public class FleeState : MonsterState
    {
        Coroutine seeking;
        event Func<IEnumerator> SeekF;
        public FleeState(Monster obj, ref Coroutine seeking, Func<IEnumerator> seekF, Func<IEnumerator> fleeing) : base(obj, fleeing)
        {
            this.seeking = seeking;
            SeekF += seekF;
        }
        public override void ToggleOn()
        {
            base.ToggleOn();
            obj.StopCoroutine(seeking);
            seeking = null;
        }
        public override void ToggleOff()
        {
            base.ToggleOff();
            seeking=obj.StartCoroutine(SeekF?.Invoke());
            
        }
    }


}