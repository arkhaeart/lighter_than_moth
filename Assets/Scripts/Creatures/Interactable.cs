﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : ObjectToSave
{

    public enum InterType
    {
        SINGLE,
        TWO_WAY,
        FLOOR_PAD
    }
    public int instanceID;
    public InterType interType;
    public LeverEffect[] effects;
    List<ILeverCommand> commands=new List<ILeverCommand>();
    List<Command> comLog = new List<Command>();
    delegate void OnLeverPulled();
    event OnLeverPulled onLeverPulled;
    public bool state = false;
    

    private void Start()
    {
        UnPackLeverCommands();
        onLeverPulled += PullResponse;
    }
    
    void UnPackLeverCommands()
    {

        foreach (var effect in effects)
        {
            switch (effect.type)
            {
                case LeverEffect.EffectType.MOVE:
                    MoveCommand command1 = new MoveCommand(effect.obj.transform.position, effect.direction);
                    commands.Add(command1);
                    break;
                case LeverEffect.EffectType.ENABLE:
                    EnableCommand command2 = new EnableCommand(effect.obj.transform.position);
                    commands.Add(command2);
                    break;
                case LeverEffect.EffectType.ACTIVATE:
                    ActivateCommand command3 = new ActivateCommand(effect.obj.transform.position);
                    commands.Add(command3);
                    break;
            }

        }

    }
    void PullResponse()
    {
        if (interType == InterType.SINGLE && state)
            return;
        foreach (var command in commands)
        {
            OnLeverEffect(command);
        }
    }
    void OnLeverEffect(ILeverCommand command)
    {
        if (state)
            command.Undo();
        else
            command.Do();
        state = !state;

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        onLeverPulled();
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if(interType==InterType.FLOOR_PAD)
            onLeverPulled();
    }
    protected override void OnDisable()
    {

        onLeverPulled -= PullResponse;
        base.OnDisable();
    }
    public void OnLoadedState(bool savedState)
    {

        onLeverPulled();
    }
}
[System.Serializable]
public class LeverEffect
{
    public enum EffectType
    {
        MOVE,
        ENABLE,
        ACTIVATE,
    }
    public EffectType type;
    public GameObject obj;
    public Vector3 direction;
}
public class LeverData
{
    public int X;
    public int Y;
    public bool state;
    public LeverData(int _X, int _Y, bool _state)
    {
        X = _X;
        Y = _Y;
        state = _state;
    }
}
public interface ILeverCommand
{
    void Do();
    void Undo();
}
public abstract class Command
{
    public Vector3 pos;
    protected GameTile GetTile
    {
        get
        {
            System.Tuple<int, int> tuple = pos.GetTileCoord();
            return GameBoard.Board.gameBoard[tuple.Item1, tuple.Item2, (int)pos.z];
        }
    }
}
public class MoveCommand :Command, ILeverCommand
{

    Vector3 direction;
    public MoveCommand(Vector3 _pos, Vector3 _direction)
    {
        pos = _pos;
        direction = _direction;
    }
    public void Do()
    {
        GetTile.Dimensions = direction;
    }

    public void Undo()
    {
        GetTile.Dimensions = -direction;
    }
}
public class EnableCommand : Command,ILeverCommand
{

    public EnableCommand (Vector3 _pos)
    {
        pos = _pos;
    }
    public void Do()
    {
        GameObject obj=GetTile.gameObject;
        obj.SetActive(!obj.activeSelf);
    }

    public void Undo()
    {
        Do();
    }
}
public class ActivateCommand : Command, ILeverCommand
{
    public ActivateCommand(Vector3 _pos)
    {
        pos = _pos;
    }
    public void Do()
    {
        GameObject obj = GetTile.gameObject;
        obj.SendMessage("SomeDumbMethod");
    }

    public void Undo()
    {
        GameObject obj = GetTile.gameObject;
        obj.SendMessage("EvenMoreDumbiestMethod");
    }
}