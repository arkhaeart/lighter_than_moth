﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Action = System.Action<float>;

namespace Creatures {
    public class Moth : MovingAgent, IPlayable
    {
        public float health = 10;
        //bool seeLight = false;
        LightSeeing lightSeeing;
        event Action HealthUpdated;
        //delegate void OnSawLight(ShiningTile tile);
        //event OnSawLight onSawLight;
        // Start is called before the first frame update
        void Start()
        {
            HealthUpdated += UIManager.Instance.HealthBarUpdate;
            health = 10;
            //StartCoroutine(CheckLight());
            //StartCoroutine(HealthChange());
            //onSawLight += TryFollowLight;
            lightSeeing = new LightFollow(this, TryFollowLight,HealthChange);
        }
        //IEnumerator CheckLight()
        //{
        //    while (true)
        //    {
        //        FloorTile current = (FloorTile)Board.GetTileFromPos(transform.position);
        //        if (current.shiningTile != null)
        //        {

        //            seeLight = true;
        //            onSawLight?.Invoke(current.shiningTile);
        //        }
        //        else
        //            seeLight = false;
        //        yield return new WaitForSeconds(0.5f);
        //    }
        //}
        void HealthChange(bool seeLight)
        {
            health = Mathf.Clamp(seeLight ? health + 0.5f : (health - 0.3f), 0, 10);
            HealthUpdated(health);
            if (health == 0)
            {
                Death();
            }

        }
        void TryFollowLight(ShiningTile tile)
        {

            if (tile.parent != null)
            {
                int x = tile.parent.x - tile.x;
                int y = tile.parent.y - tile.y;
                x = (int)Mathf.Clamp(x, -1f, 1f);
                y = (int)Mathf.Clamp(y, -1f, 1f);
                Vector3 direction = new Vector3(x, y, 0);
                if (movement == null)
                    movement = StartCoroutine(DiscreteMovement(direction));

            }
        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            Agent agent = collision.GetComponent<Agent>();
            if (agent is Monster)
                Death();
            else if(agent is Bonus bonus)
            {

            }
    }
        public void ArrivedToExit()
        {
            StartCoroutine(AwaitAnother());
        }
        public IEnumerator AwaitAnother()
        {
            onExit++;
            yield return new WaitForSeconds(1f);
            while (transform.position == LevelManager.Instance.ExitPosition)
            {
                
                if (onExit == 2)
                {
                    onExit = 0;
                    GameManager.Instance.NextLevel();
                    yield break;
                }
                yield return null;
            }
            onExit--;
        }
        void Death()
        {
            Debug.Log("DEATH!");
            //dramatic animation, sound, effects
            GameManager.Instance.GameOver();
        }
        protected override void OnDisable()
        {
            base.OnDisable();

        }
    }
}