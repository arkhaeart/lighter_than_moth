﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using GameBoard;
namespace Creatures
{
    public class Monster : MovingAgent
    {
        public float sight;
        public float angle = 90;
        public bool nightVision;
        public GameObject target;
        public Vector3 lastTarget;
        public PatrolRoute patrolRoute;
        public Path path;

        bool arrived;
        Coroutine seeking,moving;
        LayerMask mask;
        Vector3 direction;
        ShiningTile lastShine;

        MonsterState Idle, Patrol, Seek, Chase, Flee,Base;
        event System.Action<MonsterState, MonsterState> OnStateChanged;
        MonsterState currentState;
        public MonsterState CurrentState
        {
            get => currentState;
            set
            {
                MonsterState lastState=currentState;
                currentState = value;
                OnStateChanged(lastState, value);

            }
        }
        Vector3 LastTarget
        {
            get => lastTarget;
            set
            {
                ToggleMove(false);
                lastTarget = value;
            }
        }
        bool targetVisible = false;
        bool TargetVisible
        {
            get=>targetVisible;
            set
            {
                if (CurrentState != Flee&&value==true)
                    CurrentState = Chase;

                targetVisible = value;
            }
        }


        private void Start()
        {
            target = GameManager.Instance.Target;
            seeking = StartCoroutine(Seeing());
            InitStates();
            currentState = Idle;
            OnStateChanged += ToggleState;
            if (patrolRoute.patrolNodes.Length > 1)
                Base = Patrol;
            else
                Base = Idle;
            mask = LayerMask.GetMask("Walls");
            direction = Vector3.right;
            
            CurrentState = Base;
            if (nightVision)
                StartCoroutine(CheckLight());
            

        }
        void InitStates()
        {
            Idle = new IdleState(this);
            Patrol = new PatrolState(this, new System.Func<IEnumerator>(Patrolling));
            Seek=new SeekState(this,  new System.Func<IEnumerator>(SmartSeeking));
            Chase = new ChaseState(this, new System.Func<IEnumerator>(Chasing));
            Flee = new FleeState(this,ref seeking,
                new System.Func<IEnumerator>(Seeing),new System.Func<IEnumerator>(Fleeing));

     
        }
        void ToggleState(MonsterState from, MonsterState to)
        {
            if (from == to)
                return;
            Debug.Log($"State toggled to {to}");
            from.ToggleOff();
            to.ToggleOn();
        }
        protected override void Move(Vector3 direction)
        {
            this.direction = direction;
            base.Move(direction);
        }
        bool InDirection(Vector3 dif)
        {
            if (Vector3.Angle(direction, dif) <= angle / 2)
                return true;
            return false;
        }
        bool InRadius()
        {
            if (Vector3.Distance(transform.position, target.transform.position) <= sight)
                return true;
            else return false;
        }
        bool TargetIsVisible()
        {
            RaycastHit2D hit = Physics2D.Linecast(transform.position, target.transform.position, mask);
            if (hit.transform == null)
                return true;
            else return false;
        }
        void RequestPath()
        {
            PathFinder.Instance.OnPathRequested(new PathFinder.PathRequest(transform.position, lastTarget, InitMovement));
        }
        void InitMovement(Vector3[] points)
        {

            path = new Path(points);
            ToggleMove(true);
        }
        void ToggleMove(bool active)
        {
            if (active)
                moving = StartCoroutine(MovingTo());
            else if(moving!=null)
            {
                StopCoroutine(moving);
                moving = null;
            }
        }
        IEnumerator CheckLight()
        {
            while (true)
            {
                FloorTile current= Board.GetTileFromPos<FloorTile>(transform.position);
                if(current.shiningTile==null)
                {
                    CurrentState = Base;
                    lastShine = null;
                    
                }
                else if(current.shiningTile.intensity!=0)
                {
                    lastShine = current.shiningTile;
                    CurrentState = Flee;
                }
                yield return null;
            }
        }
        IEnumerator Patrolling()
        {
            arrived = true;
            int patrolIndex = 0;
            while(CurrentState==Patrol)
            {

                if (arrived)
                {
                    LastTarget = patrolRoute.patrolNodes[patrolIndex].position;
                    arrived = false;
                    patrolIndex++;
                    if (patrolIndex >= patrolRoute.patrolNodes.Length)
                        patrolIndex = 0;
                    RequestPath();
                }
                else if (transform.position == LastTarget)
                    arrived = true;
                yield return new WaitForSeconds(2.5f);
                
            }
        }
        IEnumerator MovingTo()
        {
            int currentWaypoint = 0;

                while ( path.Exists)
                {
                    if (transform.position == path.Last)
                        yield break;
                    if (movement == null)
                    {
                        movement = StartCoroutine(DiscreteMovement((path[currentWaypoint] - transform.position) / 0.16f));
                        currentWaypoint++;
                    }
                    yield return null;
                }
            
        }
        IEnumerator Chasing()
        {
            RequestPath();
            while(transform.position!=lastTarget)
            {
                if (moving == null)
                    RequestPath();
                yield return null;
            }
            if (transform.position == target.transform.position)
            {
                CurrentState = Base;
                yield return null;
            }
            CurrentState = Seek;
        }
        IEnumerator Fleeing()
        {
            int x = lastShine.x - lastShine.parent.x;
            int y = lastShine.y-lastShine.parent.y;
            LastTarget = new Vector3(x*0.16f,y*0.16f)+transform.position;
            RequestPath();
            while (transform.position != lastTarget)
            {
                yield return null;
            }
            CurrentState = Base;
        }
        IEnumerator SmartSeeking()
        {

            
            for (int i = -1; i < 2; i++)
            {
                Quaternion newQua = Quaternion.AngleAxis(45*i, Vector3.forward);
                direction = newQua * direction ;
                yield return new WaitForSeconds(1f);
            }
            if(!TargetVisible)
                CurrentState = Base;
        }
        IEnumerator Seeing()
        {
            while (true)
            {
                if (target != null)
                {
                    Vector3 dif = target.transform.position - transform.position;
                    if (InRadius())
                    {
                        if (InDirection(dif))
                        {
                            if (TargetIsVisible())
                            {
                                LastTarget = target.transform.position;
                                TargetVisible = true;
                                yield return new WaitForSeconds(0.1f);
                                continue;
                            }
                        }
                    }
                }
                TargetVisible = false;
                yield return new WaitForSeconds(0.1f);

            }
        }
    }

    [System.Serializable]
    public class PatrolRoute
    {
        public Transform[] patrolNodes;
    }
}
