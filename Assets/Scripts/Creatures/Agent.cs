﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Movement;
[RequireComponent(typeof(Rigidbody2D))]
public abstract class Agent : MonoBehaviour
{
    protected MoveModule moveModule;
    protected Rigidbody2D rb2d;
    protected virtual void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.bodyType = RigidbodyType2D.Kinematic;
        moveModule = new MoveModule(rb2d,this);
    }
    protected virtual IEnumerator SmoothMovement(Vector3 direction)
    {
        float e = 0.01f;
        Vector3 target = (transform.position + direction * 0.16f).Round();
        
        while (Vector3.Distance(transform.position,target)>e)
        {

            Vector2 newPos = transform.position + direction * 0.16f / 4;
            rb2d.MovePosition(newPos);
            yield return null;
        }
        rb2d.MovePosition(target);
    }
}
