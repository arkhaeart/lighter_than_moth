﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bonus : Agent
{
    public BonusType type;
    public void Effect(System.Action<BonusType> callback)
    {
        callback(type);
        gameObject.SetActive(false);
    }

}
public enum BonusType
{
    SPEED,
    LIGHT,
    ARMOR
}