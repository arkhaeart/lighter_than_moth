﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelExit : MonoBehaviour,IGate
{
    public void Register()
    {
        LevelManager.Instance.OnGateRegistration(this);
    }

    // Start is called before the first frame update
    void OnEnable()
    {
        LevelManager.gateRegistration += Register;
    }
    void OnDisable()
    {
        LevelManager.gateRegistration -= Register;
    }
}
public interface IGate
{
    void Register();
}
