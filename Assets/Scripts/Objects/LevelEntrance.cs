﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEntrance : MonoBehaviour,IGate
{
    public enum Belonging
    {
        LAMP,
        MOTH
    }
    public Belonging belonging;
    // Start is called before the first frame update
    void OnEnable()
    {
        LevelManager.gateRegistration+=Register;
    }
    public void Register()
    {
        LevelManager.Instance.OnGateRegistration(this);
        
    }
    void OnDisable()
    {
        LevelManager.gateRegistration -= Register;
    }

}
