﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Garbage : ObjectToSave
{
    public bool Push ( Vector3 direction)
    {

        if (Physics2D.Raycast(transform.position, direction, 0.223f).transform == null)
        {
            StartCoroutine(SmoothMovement(direction));
            return true;
        }
        else
            return false;
    }
}
