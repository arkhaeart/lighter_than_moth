﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using System.Linq;
using Tuple = System.Tuple<int, int>;
using Node = Pathfinding.Node;
namespace GameBoard {
    public class BoardBuilder
    {
        Board board;
        public BoardBuilder(Board board)
        {
            this.board = board;
        }
        public BuildResults Build()
        {
            GameTile[] tiles = GetTileList();
            KeyValuePair<int, int> pair =  CalculateDimensionsAndOffset(tiles);
            board.xLength = pair.Key;
            board.yLength = pair.Value;
            GameTile[,,]gameBoard = new GameTile[pair.Key, pair.Value, 2];
            Interactable[,]levers = new Interactable[pair.Key, pair.Value];
            Node[,] walkingGrid = new Node[pair.Key, pair.Value];
            FillGameBoard(tiles,ref gameBoard,ref walkingGrid);
            var extras = GetExtras();
            FillLevers(extras,ref levers);
            return new BuildResults(gameBoard,levers,walkingGrid);
        }
        GameTile[] GetTileList()
        {
            GameTile[]blocks =GridManager.Instance.blocks.GetComponentsInChildren<GameTile>(true);
            return blocks;

        }

        KeyValuePair<int, int> CalculateDimensionsAndOffset(GameTile[] tiles)
        {

                var Xmax = tiles.Max((n) => n.transform.position.x);
                var Xmin = tiles.Min((n) => n.transform.position.x);
                var Ymax = tiles.Max((n) => n.transform.position.y);
                var Ymin = tiles.Min((n) => n.transform.position.y);
                int X = Mathf.RoundToInt((Xmax - Xmin) / 0.16f)+1;
                int Y = Mathf.RoundToInt((Ymax - Ymin) / 0.16f)+1;
                Board.xOffset = Xmin;
                Board.yOffset = Ymin;
                return new KeyValuePair<int, int>(X, Y);

        }

        void FillGameBoard(GameTile[] gameTiles,ref GameTile[,,]gameBoard,ref Node[,]walkingGrid)
        {
            foreach (var tile in gameTiles)
            {
                Tuple tuple = tile.transform.position.GetTileCoord();
                int x = tuple.Item1;
                int y = tuple.Item2;

                if (tile is FloorTile)
                {
                    gameBoard[x, y, 0] = tile;
                    walkingGrid[x, y] = new Node(tile.transform.position, x, y);
                }
                else if (tile is WallTile)
                { gameBoard[x, y, 1] = tile;}
                
        }

        }
        void FillLevers(Interactable[] levers,ref Interactable[,] leverList)
        {
            foreach (var lever in levers)
            {
                Tuple tuple = lever.transform.position.GetTileCoord();
                int x = tuple.Item1;
                int y = tuple.Item2;
                leverList[x, y] = lever;
            }
          
        }

        Interactable[] GetExtras()
        {
            return GridManager.Instance.extras.GetComponentsInChildren<Interactable>(true);
        }
    }
    public struct BuildResults
    {
        public GameTile[,,] gameBoard;
        public Interactable[,] levers;
        public Node[,] walkingGrid;
        public BuildResults(GameTile[,,]_gameBoard,Interactable[,]_levers,Node[,]_walkingGrid)
        {
            gameBoard = _gameBoard;
            levers = _levers;
            walkingGrid = _walkingGrid;
        }
    }
}