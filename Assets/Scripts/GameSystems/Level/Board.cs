﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using System.Linq;
using Tuple = System.Tuple<int,int>;
using Pathfinding;
namespace GameBoard
{
    public class Board : Singleton<Board>
    {
        public static GameTile[,,] gameBoard;
        public static Interactable[,] levers;
        public static float xOffset, yOffset;
        //public GameTile[] gameTiles;
        [HideInInspector] public int xLength, yLength;
        static LayerMask mask;
        Node[,] walkingGrid;

        public void Init()
        {
            BoardBuilder builder = new BoardBuilder(this);
            var results=builder.Build();
            UnpackResults(results);
            mask = LayerMask.GetMask("Walls");
        }
        void UnpackResults(BuildResults results)
        {
            gameBoard = results.gameBoard;
            levers = results.levers;
            walkingGrid = results.walkingGrid;
        }

        public Node GetNodeFromPos(Vector3 pos)
        {
            Tuple tuple = pos.GetTileCoord();
            try { return walkingGrid[tuple.Item1, tuple.Item2]; }
            catch(System.ArgumentOutOfRangeException e)
            {
                return null;
            }
        }
        public List<Node> GetNeighbours(Node node)
        {
            List<Node> neighbours=new List<Node>();
            for (int i = -1; i <= 1; i++)
            {
                for (int y = -1; y <= 1; y++)
                {
                    if (i == 0 && y == 0)
                        continue;
                    int checkX = node.X + i;
                    int checkY = node.Y + y;
                    if (checkX >= 0 && checkX < xLength && checkY >= 0 && checkY < yLength && walkingGrid != null)
                    {
                        if (walkingGrid[checkX,checkY]!=null)
                            neighbours.Add(walkingGrid[checkX, checkY]);
                    }
                }
            }
            return neighbours;
        }

        public int GetTileCount()
        {
            return xLength * yLength;
        }
        public static T GetTileFromPos<T>(Vector3 pos)where T:GameTile
        {
            Tuple tuple = pos.GetTileCoord();
            int x = tuple.Item1;
            int y = tuple.Item2;

            GameTile tile = gameBoard[x, y, 1];
            if (tile == null)
                tile = gameBoard[x, y, 0];
            return (T)tile;
        }
        public static void OnTileMoved(GameTile tile, Vector3 dim)
        {
            Tuple tuple = tile.transform.position.GetTileCoord();
            int x = tuple.Item1;
            int y = tuple.Item2;
            if (gameBoard[x, y, 1] != null && tile is WallTile)
            {
                gameBoard[x, y, 1] = null;
                gameBoard[x + (int)dim.x, y + (int)dim.y, 1] = tile;
            }
            else if (tile is FloorTile)
            {
                gameBoard[x, y, 0] = null;
                gameBoard[x + (int)dim.x, y + (int)dim.y, 0] = tile;
            }
        }

        public static bool CheckWalkable(float x, float y)
        {
            Vector3 pos = new Vector3(x, y, 0);
            GameTile tile = GetTileFromPos<GameTile>(pos);
            if (tile is FloorTile)
                return true;
            else return false;
        }
        public static bool CheckLight(Vector3 pos, Vector3 dir)
        {

            RaycastHit2D hit = Physics2D.Raycast(pos, dir - pos, 0.142f, mask);
            if (hit.transform != null)
            {
                return true;
            }

            return false;
        }
        public static void LightResponse(ShiningTile shTile)
        {
            int x = shTile.x;
            int y = shTile.y;
            float mod = shTile.intensity;
            GameTile tile = gameBoard[x, y, 1];
            if (tile == null)
            {

                FloorTile ftile = (FloorTile)gameBoard[x, y, 0];
                if (ftile == null)
                    return;
                if (mod == 0)
                    ftile.shiningTile = null;
                else
                {
                    ftile.shiningTile = shTile;
                }
                ftile.ChangeColor(0.33f + mod * 0.22f);

            }
        }
    }
}
