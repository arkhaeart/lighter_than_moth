﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using GameBoard;

public class LevelManager : Singleton<LevelManager>
{
    public Board board;
    public List<LevelEntrance> gates=new List<LevelEntrance>();
    public delegate void GateRegistration();
    public static event GateRegistration gateRegistration;
    public Vector3 ExitPosition { get; private set; }
    public int sceneID;
    protected override  void Awake()
    {
        base.Awake();
        board = GetComponent<Board>();
        //board.CreateGameBoard();
        gates.Clear();
    }

    public void OnGateRegistration(IGate gate)
    {
        if(!gates.Contains(gate)&&gate is LevelEntrance)
        {
            LevelEntrance entrance = (LevelEntrance)gate;
            gates.Add(entrance);
        }
        else if(gate is LevelExit)
        {
                LevelExit exit = (LevelExit)gate;
                ExitPosition = exit.transform.position;
                Debug.Log(ExitPosition);
        }
    }
    public LevelEntrance GetEntrance(LevelEntrance.Belonging belonging)
    {
        if(gates.Count!=3)
        {
            gateRegistration();
        }
        
        List<LevelEntrance> entrances = gates.Where(n => n is LevelEntrance).ToList();
        return entrances.Single(n => n.belonging == belonging);
    }
}
