﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Action = System.Action;

public class SaveLoadMenu : Singleton<SaveLoadMenu>
{
    public enum State { SAVE,LOAD}
    public GameObject confirmWindow;
    public Text[] saveText;
    public State state;
    Action toPerform;
    private void OnEnable()
    {
        OnDrawSaves();
    }
    public void OnDrawSaves()
    {
        SaveLoadManager.Instance.Init();
        string[] saves=SaveLoadManager.Instance.GetSaveInfo();
        for (int i = 0; i < saveText.Length; i++)
        {
            Debug.Log(saves[i]);
            saveText[i].text = saves[i];
            if(state==State.LOAD)
                saveText[i].GetComponentInParent<Button>().interactable = saves[i] == "Empty Save Slot" ? false : true;
        }
    }
    public void OnButtonResponse(int index)
    {
        confirmWindow.SetActive(true);
        if(state==State.LOAD)
        {
            toPerform +=()=> MainMenuManager.Instance.OnGameLoad(index);
        }
        else
        {
            toPerform += () => SaveLoadManager.Instance.SaveGame(index);
        }
    }
    public void OnConfirmedChoice()
    {
        toPerform?.Invoke();
        toPerform = null;
        
    }
    public void OnRejected()
    {
        confirmWindow.SetActive(false);
        toPerform = null;
    }
    private void OnDisable()
    {
        toPerform = null;
    }
}
