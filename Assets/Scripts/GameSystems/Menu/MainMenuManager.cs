﻿using System.Collections;
using Action=System.Action;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuManager : Singleton<MainMenuManager>,IMenuToggle
{
    public GameObject mainMenu, levelMenu,  loadMenu, exitMenu;
    public GameObject lvlButton,lvlparent;
    GameObject lastMenu;
    Action currentToggle;
    MenuEnum current;
    private void Start()
    {
        currentToggle+=()=>ToggleMenu(mainMenu);
    }
    public void StartNewGame()
    {

        Debug.Log(GameManager.Instance.DataSaver);
        GameManager.loadMode = GameManager.LoadMode.BLANK;
        GameManager.Instance.DataSaver.EraseData();
        SceneManager.LoadScene(1, LoadSceneMode.Single);

    }
    public MenuEnum Current
    {
        get { return current; }

        set
        {

            GameObject obj=null;
            switch((int)value)
            {
                case 0:
                    obj = mainMenu;
                    break;
                case 1:
                    obj = loadMenu;
                    break;
                case 2:
                    obj = levelMenu;
                    currentToggle += () => OnDrawLevels();
                    break;
                case 3:
                    obj = exitMenu;
                    break;
            }
            currentToggle += () => ToggleMenu(obj);
            currentToggle?.Invoke();
            currentToggle = null;
            currentToggle += () => ToggleMenu(obj);
            lastMenu = obj;
            current = value;
        }
    }
    public void ToggleMenu(GameObject obj)
    {
        obj.SetActive(!obj.activeSelf);
    }
    public void OnDrawLevels()
    {

        for (int i = 0; i < GameManager.Instance.DataSaver.levels.Length; i++)
        {
            LevelData data = GameManager.Instance.DataSaver.levels[i];
            GameObject obj = Instantiate(lvlButton, lvlparent.transform);
            obj.transform.Find("LevelName").GetComponent<Text>().text = data.name;
            obj.transform.Find("HighScore").GetComponent<Text>().text = "High Score: "+data.highScore;
            obj.GetComponent<Button>().interactable = data.opened;
            obj.GetComponent<LvlButton>().index = i + 1;
        }
    }
    public void OnLevelChosen(int index)
    {
        GameManager.loadMode = GameManager.LoadMode.BLANK;
        SceneManager.LoadScene(index,LoadSceneMode.Single);
    }
    public void OnMenuToggle(int index)
    {
        Current = (MenuEnum)index;
    }
    public void OnGameExit()
    {
        Debug.Log("exitting game!");
        Application.Quit();
    }
    public void OnGameLoad(int index)
    {
        GameManager.loadMode = GameManager.LoadMode.SAVED;
        SaveLoadManager.Instance.LoadGame(index);
    }
    private void OnDisable()
    {
        currentToggle = null;
    }
}
public interface IMenuToggle
{
    MenuEnum Current { get; set; }
}
public enum MenuEnum
{
    MAIN,
    FIRST,
    SECOND,
    THIRD,
    FOURTH
}