﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;

public class SaveLoadManager : Singleton<SaveLoadManager>
{
    public delegate void OnGameSaved();
    public static event OnGameSaved onGameSaved;
    public string[] saveFile;
    public List<ObjectToSave> objects;
    string sav = "saves/";
    public void Init()
    {
        saveFile = new string[4];
        GetOrCreateDirectory();
    }
    public void SaveGame(int index)
    {
        onGameSaved?.Invoke();
        using (FileStream fs = File.Create(sav+ "save_"+index.ToString()+".sav"))
        {
            
            BinaryWriter bs = new BinaryWriter(fs);
            bs.Write(LevelManager.Instance.sceneID);
            
            var tiles =
                from @object in objects
                where @object is MovingAgent
                select @object;
            bs.Write(tiles.Count());
            foreach(var obj1 in tiles)
            {
                bs.Write(obj1.ID);
                bs.Write(obj1.Dimensions.Item1);
                bs.Write(obj1.Dimensions.Item2);
                
            }
            var levers = objects.Except(tiles);
            bs.Write(levers.Count());
            foreach (var obj2 in levers)
            {
                bs.Write(obj2.ID);
                bs.Write(obj2.Dimensions.Item1);
                bs.Write(obj2.Dimensions.Item2);
                var lever = (Interactable)obj2;
                bs.Write(lever.state);

            }
        }
    }
    public void LoadGame(int index)
    {
        if(!File.Exists(saveFile[index]))
        {
            Debug.LogError("Cannot find save file!");
            return;
        }
        using (FileStream fs = File.OpenRead(saveFile[index]))
        {
            BinaryReader br = new BinaryReader(fs);
            int sceneID = br.ReadInt32();
            int objCount = br.ReadInt32();
            for (int i = 0; i < objCount; i++)
            {
                UnpackObj(br);
            }
            int leverCount = br.ReadInt32();
            GameManager.loadMode = GameManager.LoadMode.SAVED;
            GameManager.Instance.LoadLevel(sceneID);
            for (int i = 0; i < leverCount; i++)
            {
                UnpackLever(br);
            }



        }
    }
    public string[] GetSaveInfo()
    {
        string[] saveInfo = new string[4];
        for (int i = 0; i < saveInfo.Length; i++)
        {
            if (!File.Exists(saveFile[i]))
            {
                saveInfo[i] = "Empty Save Slot";
                continue;
            }
            var subSaveInfo = new string[3];
            subSaveInfo[0] = saveFile[i];
            subSaveInfo[1] = Directory.GetLastWriteTime(saveFile[i]).ToString();
            using (FileStream fs = File.OpenRead(saveFile[i]))
            {
                BinaryReader br = new BinaryReader(fs);
                subSaveInfo[2]="Level "+br.ReadInt32().ToString();
            }
            saveInfo[i] = string.Join(" ", subSaveInfo);
        }
        return saveInfo;
    }
    void UnpackObj(BinaryReader br)
    {
        int ID = br.ReadInt32();
        int X = br.ReadInt32();
        int Y = br.ReadInt32();
        PrefabData prefab = GameManager.Instance.DataSaver.prefabs[ID];
        PrefabData newPrefab = new PrefabData(prefab, X, Y);
        GameManager.Instance.prefabs.Add(newPrefab);
    }
    void UnpackLever(BinaryReader br)
    {
        int ID = br.ReadInt32();
        int X = br.ReadInt32();
        int Y = br.ReadInt32();
        bool state = br.ReadBoolean();
        LeverData leverData = new LeverData(X, Y, state);
        GameManager.Instance.levers.Add(leverData);

    }
    void GetOrCreateDirectory()
    {
        string[] saveInfo = new string[4];
        if (!Directory.Exists(sav))
        {
            Directory.CreateDirectory(sav);
        }
        var saves=Directory.GetFiles(sav);
        for (int i =0;i<saves.Length;i++)        
        {
            string save = Path.GetFileNameWithoutExtension(saves[i]);
            for (int y = 0; y < saveFile.Length; y++)
            {
                if (save.EndsWith(y.ToString()))
                {
                    saveFile[y] =  saves[i];
                }
            }
        } 

    }
}

//Save following data- level number, moth health, all vector3 of all creatures,