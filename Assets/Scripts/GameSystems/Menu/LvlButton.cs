﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LvlButton : MonoBehaviour
{

    public int index;
    public void OnClick()
    {
        MainMenuManager.Instance.OnLevelChosen(index);
    }
}
