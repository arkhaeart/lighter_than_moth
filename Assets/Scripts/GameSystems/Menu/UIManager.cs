﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIManager : Singleton<UIManager>
{
    delegate void TogglePause();
    delegate void SaveGame(int i);
    event SaveGame saveGame;
    event TogglePause togglePause;
    public Slider slider;
    public GameObject pauseMenu,saveSlotsMenu;
    private void OnEnable()
    {
        togglePause += Pause;

    }
    private void Start()
    {
            saveGame += SaveLoadManager.Instance.SaveGame;
    }
    private void OnDisable()
    {
        togglePause=null;
    }
    private void Update()
    {
        if (Input.GetButtonDown("Escape"))
        {
            togglePause();
        }
    }
    void Pause()
    {
        pauseMenu.SetActive(!pauseMenu.activeSelf);
        Time.timeScale = Mathf.Abs(Time.timeScale - 1f);
    }
    public void HealthBarUpdate(float value)
    {
        slider.value = value;
    }
    public void OnMenuToggle()
    {
        saveSlotsMenu.SetActive(!saveSlotsMenu.activeSelf);
    }
    public void OnButtonResponse()
    {
        Time.timeScale = 1;
        GameManager.Instance.ToMainMenu();
    }
    public void OnButtonResponse(int index)
    {
        saveGame(index);
    }

}
