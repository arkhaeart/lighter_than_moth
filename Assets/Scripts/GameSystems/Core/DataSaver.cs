﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName ="DataSaver")]
public class DataSaver : ScriptableObject
{
    public LevelData[] levels;
    public PrefabData[] prefabs;
    public void WriteData(int index, float highscore)
    {
        levels[index-1].highScore = highscore;
        if(levels.Length>=index+1)
        {
            levels[index].opened = true;
        }
    }
    public void EraseData()
    {
        for (int i = 1; i < levels.Length-1; i++)
        {
            levels[i].opened = false;
            levels[i].highScore = 0;
        }
        levels[0].highScore = 0;
    }
}
[System.Serializable]
public class LevelData
{
    public string name;
    public bool opened=false;
    public float highScore=0;
}
[System.Serializable]
public class PrefabData
{
    public GameObject obj;
    public int X, Y;
    public PrefabData(PrefabData parent,int _X, int _Y)
    {
        obj = parent.obj;
        X = _X;
        Y = _Y;
    }
}
