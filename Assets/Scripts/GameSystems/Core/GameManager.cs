﻿using System.Collections;
using System.Threading.Tasks;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using GameBoard;

public class GameManager : Singleton<GameManager>
{
    public enum LoadMode { BLANK,SAVED }
    public static LoadMode loadMode;
    public List<PrefabData> prefabs = new List<PrefabData>();
    public List<LeverData> levers = new List<LeverData>();

    GameObject target;
    DataSaver dataSaver;
    [SerializeField] private GameObject lamp, moth;
    
    private GameObject Lamp
    { get
        {
            if(lamp==null)
            {
                lamp = Resources.Load("Prefabs/Lamp") as GameObject;
            }
            return lamp;
        }
    }
    private GameObject Moth
    {
        get
        {
            if(moth==null)
            {
                moth = Resources.Load("Prefabs/Moth") as GameObject;
            }
            return moth;
        }
    }
    public GameObject Target => target;
    public DataSaver DataSaver
    {
        get
        {
            if (dataSaver == null)
            {
                dataSaver = Resources.Load("DataSaver") as DataSaver;
            }
            return dataSaver;
        }
    }
    bool loading=false;
    protected override void Awake()
    {
        base.Awake();
        DontDestroyOnLoad(gameObject);
    }
    private void Start()
    {

    }
    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }
    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        loading = false;
        if(scene.buildIndex!=0)
            OnStart();
    }
    private void OnStart()
    {
        try
        {
            Board.Instance.Init();
           
        }
        catch(System.Exception e)
        {
            Debug.Log( e.GetBaseException());
            throw e;
        }
        

        if (loadMode == LoadMode.BLANK)
        {
            LevelEntrance mEntrance = LevelManager.Instance.GetEntrance(LevelEntrance.Belonging.MOTH);
            LevelEntrance lEntrance = LevelManager.Instance.GetEntrance(LevelEntrance.Belonging.LAMP);
            GameObject newLamp = Instantiate(Lamp, lEntrance.transform.position, Quaternion.identity) as GameObject;
            newLamp.transform.SetParent(GridManager.Instance.units.transform);
            GameObject newMoth = Instantiate(Moth, mEntrance.transform.position, Quaternion.identity) as GameObject;
            newMoth.transform.SetParent(GridManager.Instance.units.transform);
            target = newMoth;
        }
        else
        {

            foreach(var prefab in prefabs)
            {
                GameObject newObj = Instantiate(prefab.obj, Board.gameBoard[prefab.X, prefab.Y, 0].transform.position, Quaternion.identity) as GameObject;
                newObj.transform.SetParent(GridManager.Instance.units.transform);
            }
            foreach(var lever in levers)
            {
                Interactable curLever = Board.levers[lever.X, lever.Y];
                if (curLever == null || !lever.state)
                    continue;
                curLever.OnLoadedState(true);
            }
        }
    }
    public void NextLevel()
    {
        loadMode = LoadMode.BLANK;
        if (loading)
            return;
        loading = true;
        int level = LevelManager.Instance.sceneID;
        DataSaver.WriteData(level, 100);
        if((level+1)>DataSaver.levels.Length)
        {
            GameWon();
            return;
        }

        LoadLevel(level + 1);
    }
    public void LoadLevel(int index)
    {
        SceneManager.LoadScene(index, LoadSceneMode.Single);
    }
    public void ToMainMenu()
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
    void GameWon()
    {
        Debug.Log("You won the game! Congratulations!");
        Invoke("ToMainMenu", 3);
    }
    public void GameOver()
    {
        //Some gameoverscrript
        Invoke("ToMainMenu", 3);
    }
}
