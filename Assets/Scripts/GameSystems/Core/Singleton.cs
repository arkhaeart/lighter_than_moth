﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T:Singleton<T>
{
    public static T Instance { get; private set; }
    // Start is called before the first frame update
    protected virtual void Awake()
    {
        if (Instance == null)
            Instance = (T)this;
        if (Instance != this)
            Destroy(gameObject);
    }
    private void OnDisable()
    {
        if(Instance==this)
            Instance = null;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
